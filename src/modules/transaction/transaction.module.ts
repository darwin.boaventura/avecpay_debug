import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Transaction } from '@transaction/transaction.entity';
import { TransactionController } from '@transaction/transaction.controller';
import { AccountModule } from '@account/account.module';
import { SplitModule } from '@split/split.module';
import { FeeModule } from '@fee/fee.module';
import { TransactionService } from '@transaction/transaction.service';
import { ZoopModule } from '@zoop/zoop.module';

@Module({
  imports: [TypeOrmModule.forFeature([Transaction]), AccountModule, SplitModule, FeeModule, ZoopModule],
  exports: [TypeOrmModule, TransactionService],
  providers: [TransactionService],
  controllers: [TransactionController],
})
export class TransactionModule {}
