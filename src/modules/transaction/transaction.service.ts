import { _, get } from 'lodash';
import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Transaction } from '@transaction/transaction.entity';
import { AccountService } from '@account/account.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransactionStatusEnum } from '@transaction/enum/transactionStatus.enum';

@Injectable()
export class TransactionService {
  constructor(
    private readonly accountService: AccountService,
    @InjectRepository(Transaction) private repositoryTransaction: Repository<Transaction>,
  ) {}

  async createOne(account, data: any) {
    const transaction = new Transaction();

    if (account) {
      transaction.account = account;
    }

    try {
      const transactionUpdatedAt = get(data, 'updated_at') || get(data, 'voided_at') || get(data, 'reversed_at');

      transaction.transaction = data.id;
      transaction.transactionExpectedOn = data.expected_on as any;
      transaction.transactionCreatedAt = data.created_at as any;
      transaction.transactionUpdatedAt = transactionUpdatedAt as any;
      transaction.transactionStatus = TransactionStatusEnum[data.status] as any;
      transaction.transactionValue = data.amount;
      transaction.originalValue = data.original_amount;
      transaction.currency = data.currency;
      transaction.description = data.description;
      transaction.paymentType = data.payment_type;
      transaction.transactionNumber = data.transaction_number;
      transaction.gatewayAuthorizer = data.gateway_authorizer;
      transaction.appTransactionUid = data.app_transaction_uid;
      transaction.refunds = data.refunds;
      transaction.rewards = data.rewards;
      transaction.preAuthorization = data.pre_authorization;
      transaction.salesReceipt = data.sales_receipt;
      transaction.onBehalfOf = data.on_behalf_of;
      transaction.customer = data.customer;
      transaction.statementDescriptor = data.statement_descriptor;
      transaction.installmentNumber = get(data, 'installment_plan.number_installments', 1);
      transaction.feeTotalValue = data.fees;

      transaction.paymentMethodId = get(data, 'payment_method.id', null);
      transaction.paymentResource = get(data, 'payment_method.resource', null);
      transaction.paymentDescription = get(data, 'payment_method.description', null);
      transaction.paymentCardBrand = get(data, 'payment_method.card_brand', null);
      transaction.paymentCardFirst4Digits = get(data, 'payment_method.first4_digits', null);
      transaction.paymentCardExpirationMonth = get(data, 'payment_method.expiration_month', null);
      transaction.paymentCardExpirationYear = get(data, 'payment_method.expiration_year', null);
      transaction.paymentCardHolderName = get(data, 'payment_method.holder_name', null);

      transaction.paymentCardActive = get(data, 'payment_method.is_active', null);
      transaction.paymentCardValid = get(data, 'payment_method.is_valid', null);
      transaction.paymentCardVerified = get(data, 'payment_method.is_verified', null);
      transaction.paymentCardCustomer = get(data, 'payment_method.customer', null);
      transaction.paymentCardFingerprint = get(data, 'data.payment_method.fingerprint', null);
      transaction.paymentCardAddress = get(data, 'data.payment_method.address', null);
      transaction.pointOfSaleEntryMode = get(data, 'point_of_sale.entry_mode', null);
      transaction.pointOfSaleIdentificationNumber = get(data, 'point_of_sale.identification_number', null);
      transaction.paymentAuthorizerId = get(data, 'payment_authorization.authorizer_id', null);
      transaction.paymentAuthorizationNsu = get(data, 'payment_authorization.authorization_nsu', null);

      transaction.invoiceId = get(data, 'invoice_id', null);
      transaction.referenceId = get(data, 'reference_id', null);

      return await this.repositoryTransaction.save(transaction);
    } catch (error) {
      console.log(error);
      console.log(data);
    }
  }

  async findOneByZoopTransactionId(id: any) {
    return await this.repositoryTransaction.findOne({ where: { transaction: id } });
  }

  async updateTransaction(transaction: Transaction, data: any) {
    try {
      transaction.transactionStatus = TransactionStatusEnum[data.status] as any;

      return await this.repositoryTransaction.save(transaction);
    } catch (e) {
      const message = _.get(e, 'message') || _.get(e, 'statusText');
      const code = _.get(e, 'status') || _.get(e, 'statusCode');

      throw new HttpException(message, code);
    }
  }

  async createOrUpdateTransaction(data: any): Promise<Transaction> {
    const transactionId = get(data, 'id', null);
    const onBehalfOf = get(data, 'on_behalf_of');
    const account = (await this.accountService.getAccountByZoopId(onBehalfOf)) || null;

    if (!transactionId) {
      throw new HttpException(`No transaction ID`, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    const transaction = await this.findOneByZoopTransactionId(transactionId);

    if (transaction === undefined) {
      return await this.createOne(account, data);
    }

    return await this.updateTransaction(transaction, data);
  }
}
