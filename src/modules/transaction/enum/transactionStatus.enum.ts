export enum TransactionStatusEnum {
  new = 1,
  pending = 2,
  pre_authorized = 3,
  succeeded = 4,
  failed = 5,
  reversed = 6,
  canceled = 7,
  refunded = 8,
  dispute = 9,
  charged_back = 10,
}
