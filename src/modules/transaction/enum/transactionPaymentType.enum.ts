export enum TransactionPaymentTypeENUM {
  debit = 1,
  credit = 2,
  boleto = 3,
  bank_account = 4,
  anticipation = 5,
}
