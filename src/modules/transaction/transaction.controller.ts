import * as _ from 'lodash';
import { Controller, Post, Body, HttpStatus, Param } from '@nestjs/common';
import { Transaction } from '@transaction/transaction.entity';
import { TransactionService } from '@transaction/transaction.service';
import { AccountService } from '@account/account.service';
import { SplitService } from '@split/split.service';
import { TransactionPaymentTypeENUM } from './enum/transactionPaymentType.enum';
import { ZoopFlagCardENUM } from '@zoop/enum/zoopFlagCard.enum';
import { FeeService } from '@fee/fee.service';
import { ZoopService } from '@zoop/zoop.service';

@Controller('transaction')
export class TransactionController {
  constructor(
    private readonly accountService: AccountService,
    private readonly transactionService: TransactionService,
    private readonly splitService: SplitService,
    private readonly feeService: FeeService,
    private readonly zoopService: ZoopService,
  ) {}

  @Post('split')
  async split(@Body() params: any) {
    let account;

    const data = params;
    const zoopId = _.get(data, 'onBehalfOf');

    account = _.get(data, 'account', undefined);

    if (!account) {
      account = await this.accountService.getAccountByZoopId(zoopId);
    }

    if (!account) {
      throw {
        HAPPEN_WHEN: 'WHEN_TRYING_TO_FIGURE_OUT_ACCOUNT_BY_ZOOP_ID',
        DATA: {
          ZOOP_ID: zoopId,
          TRANSACTION_ID: _.get(data, 'transaction'),
        },
      };
    }

    const allowSplit = _.get(account, 'allowSplit', 0);

    if (allowSplit) {
      const transactionId = _.get(data, 'id', 0);
      const transactionHasSplit = await this.splitService.transactionHasSplit(transactionId);

      if (transactionHasSplit) {
        return true;
      }

      let feeToApply;

      const accountId = _.get(account, 'id');

      const paymentType: any = TransactionPaymentTypeENUM[_.get(data, 'paymentType')];
      const installmentNumber = _.get(data, 'installmentNumber');
      const paymentCardBrand = _.get(data, 'paymentCardBrand');
      const zoopFee = _.get(data, 'feeTotalValue');
      const flagCardId = ZoopFlagCardENUM[paymentCardBrand] || ZoopFlagCardENUM.unknown;

      let paymentFeePorcentage;
      let paymentFeeValue;

      paymentFeePorcentage = await this.feeService.getAFee({
        active: 1,
        paymentType,
        accountId,
        installmentNumber,
        flagCardId,
      } as any);

      if (!paymentFeePorcentage) {
        paymentFeePorcentage = await this.feeService.getAFee({
          active: 1,
          paymentType,
          accountId: 1,
          installmentNumber,
          flagCardId,
        } as any);
      }

      paymentFeePorcentage = _.get(paymentFeePorcentage, 'feeValue');

      if (paymentFeePorcentage < 1) {
        switch (paymentType) {
          // DEBIT
          case 1:
            paymentFeePorcentage = 2;
          // CREDIT
          case 2:
            paymentFeePorcentage = 3;
          // ANTICIPATION
          case 5:
            paymentFeePorcentage = 2.5;
          default:
            paymentFeePorcentage = 3;
        }
      }

      if (!paymentFeePorcentage) {
        throw {
          HAPPEN_WHEN: 'WHEN_TRYING_TO_FIGURE_OUT_PAYMENT_FEE_PORCENTAGE',
          DATA: {
            ACCOUNT_ID: accountId,
            FLAG_CARD_ID: flagCardId,
            PAYMENT_TYPE: paymentType,
            INSTALLMENT_NUMBER: installmentNumber,
            TRANSACTION_ID: _.get(data, 'transaction'),
          },
        };
      }

      const amount = _.get(data, 'originalValue');

      paymentFeeValue = (paymentFeePorcentage * amount) / 100;

      const applyAnticipationFee = _.get(account, 'automaticAnticipation') && _.get(data, 'paymentType') === 'credit';

      let anticipationFeePorcentage;

      if (applyAnticipationFee) {
        anticipationFeePorcentage = await this.feeService.getAFee({
          active: 1,
          paymentType: TransactionPaymentTypeENUM.anticipation,
          accountId,
        } as any);

        if (!anticipationFeePorcentage) {
          anticipationFeePorcentage = await this.feeService.getAFee({
            active: 1,
            paymentType: TransactionPaymentTypeENUM.anticipation,
            accountId: 1,
          } as any);
        }

        if (!anticipationFeePorcentage) {
          anticipationFeePorcentage = await this.feeService.getAFee({
            active: 1,
            paymentType: TransactionPaymentTypeENUM.anticipation,
            accountId: 1,
          } as any);
        }

        if (!anticipationFeePorcentage) {
          throw {
            HAPPEN_WHEN: 'WHEN_TRYING_TO_FIGURE_OUT_ANTICIPATION_FEE_PORCENTAGE',
            DATA: {
              ACCOUNT_ID: accountId,
              FLAG_CARD_ID: flagCardId,
              PAYMENT_TYPE: TransactionPaymentTypeENUM.anticipation,
              TRANSACTION_ID: _.get(data, 'transaction'),
            },
          };
        }

        anticipationFeePorcentage = _.get(anticipationFeePorcentage, 'feeValue');

        let totalFee = 0;

        const amountLessFee = amount - paymentFeeValue;
        const amountByInstallment = amountLessFee / installmentNumber;

        for (let i = 1; i <= installmentNumber; i++) {
          const installmentFee = anticipationFeePorcentage * i;

          totalFee += (installmentFee * amountByInstallment) / 100;
        }

        feeToApply = totalFee + paymentFeeValue;
      } else {
        feeToApply = paymentFeeValue;
      }

      feeToApply -= zoopFee;

      return {
        TRANSACTION_ID: _.get(data, 'transaction'),
        TRANSACTION_VALUE: amount,
        ANTICIPATION_IS_APPLIED: applyAnticipationFee,
        ANTICIPATION_FEE_PERCENTAGEM: anticipationFeePorcentage,
        TRANSACTION_ZOOP_FEE: zoopFee,
        FEE_PERCENTAGEM: paymentFeePorcentage,
        FEE_VALUE: paymentFeeValue,
        FEE_APPLIED: feeToApply,
      };
    }
  }

  @Post('split/:transaction')
  async splitTransaction(@Param() params) {
    const transaction = await this.zoopService.getTransactionDetails(params.transaction);

    return await this.callback(transaction);
  }

  @Post('callback')
  async callback(@Body() params: any) {
    const transaction: Transaction = await this.transactionService.createOrUpdateTransaction(params);

    if (transaction) {
      const transactionType = _.get(transaction, 'pointOfSaleEntryMode', undefined);

      const transactionTypesToSplit = ['chip', 'magstripe'];

      if (transactionTypesToSplit.includes(transactionType)) {
        const zoopId = _.get(transaction, 'onBehalfOf');
        const account = await this.accountService.getAccountByZoopId(zoopId);

        if (_.get(account, 'allowSplit', 0)) {
          return await this.split(transaction);
        }
      }

      return { data: transaction, status: HttpStatus.OK };
    }

    return { data: { message: 'Unable to process payload' }, status: 422 };
  }
}
