import { Column, Entity, Index, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { Account } from '@account/account.entity';
import { Split } from '@split/split.entity';

@Entity('transaction')
@Index('ix_transaction_account_id', ['account'])
@Index('ix_transaction_transaction', ['transaction'])
@Index('ix_transaction_transaction_expected_on', ['transactionExpectedOn'])
export class Transaction {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
  })
  id: number;

  @ManyToOne(
    (type) => Account,
    (account) => account.transactions,
    {
      nullable: true,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'account_id' })
  account: Account | null;

  @RelationId((transaction: Transaction) => transaction.account)
  accountId: number | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'transaction',
  })
  transaction: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'transaction_expected_on',
  })
  transactionExpectedOn: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'transaction_created_at',
  })
  transactionCreatedAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'transaction_updated_at',
  })
  transactionUpdatedAt: Date;

  @Column('int', {
    nullable: false,
    name: 'transaction_status',
  })
  transactionStatus: number;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'transaction_value',
  })
  transactionValue: string;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'original_value',
  })
  originalValue: string;

  @Column('varchar', {
    nullable: true,
    length: 3,
    name: 'currency',
  })
  currency: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'description',
  })
  description: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_type',
  })
  paymentType: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'transaction_number',
  })
  transactionNumber: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'gateway_authorizer',
  })
  gatewayAuthorizer: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'app_transaction_uid',
  })
  appTransactionUid: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'refunds',
  })
  refunds: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'rewards',
  })
  rewards: string | null;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'discounts',
  })
  discounts: string;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'pre_authorization',
  })
  preAuthorization: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'sales_receipt',
  })
  salesReceipt: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'on_behalf_of',
  })
  onBehalfOf: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'customer',
  })
  customer: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'statement_descriptor',
  })
  statementDescriptor: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_method_id',
  })
  paymentMethodId: string | null;

  @Column('varchar', {
    nullable: false,
    length: 50,
    name: 'payment_resource',
  })
  paymentResource: string;

  @Column('varchar', {
    nullable: true,
    name: 'payment_description',
  })
  paymentDescription: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_card_brand',
  })
  paymentCardBrand: string | null;

  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'payment_card_first4_digits',
  })
  paymentCardFirst4Digits: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'payment_card_expiration_month',
  })
  paymentCardExpirationMonth: string | null;

  @Column('varchar', {
    nullable: true,
    length: 4,
    name: 'payment_card_expiration_year',
  })
  paymentCardExpirationYear: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'payment_card_holder_name',
  })
  paymentCardHolderName: string | null;

  @Column('boolean', {
    nullable: false,
    default: () => '0',
    name: 'payment_card_active',
  })
  paymentCardActive: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => '1',
    name: 'payment_card_valid',
  })
  paymentCardValid: boolean;

  @Column('boolean', {
    nullable: false,
    default: () => '0',
    name: 'payment_card_verified',
  })
  paymentCardVerified: boolean;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_card_customer',
  })
  paymentCardCustomer: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'payment_card_fingerprint',
  })
  paymentCardFingerprint: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_card_address',
  })
  paymentCardAddress: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'point_of_sale_entry_mode',
  })
  pointOfSaleEntryMode: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'point_of_sale_identification_number',
  })
  pointOfSaleIdentificationNumber: string | null;

  @Column('int', {
    nullable: false,
    default: () => '1',
    name: 'installment_number',
  })
  installmentNumber: number;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'external_id',
  })
  externalId: string | null;

  @Column('timestamp', {
    nullable: true,
    name: 'external_id_created_at',
  })
  externalIdCreatedAt: Date | null;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'fee_total_value',
  })
  feeTotalValue: string;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_authorizer_id',
  })
  paymentAuthorizerId: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_authorization_code',
  })
  paymentAuthorizationCode: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'payment_authorization_nsu',
  })
  paymentAuthorizationNsu: string | null;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
  })
  updatedAt: Date;

  @Column('varchar', {
    nullable: true,
    name: 'reference_id',
  })
  referenceId: string;

  @Column('varchar', {
    nullable: true,
    name: 'invoice_id',
  })
  invoiceId: string;

  // Relations
  @OneToMany(
    (type) => Split,
    (transactionSplit) => transactionSplit.transaction,
    {
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
    },
  )
  transactionSplits: Split[];
}
