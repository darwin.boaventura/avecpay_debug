import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fee } from '@fee/fee.entity';
import { FeeService } from '@fee/fee.service';

@Module({
  imports: [TypeOrmModule.forFeature([Fee])],
  exports: [TypeOrmModule, FeeService],
  providers: [FeeService],
  controllers: [],
})
export class FeeModule {}
