import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity('fee')
export class Fee {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
  })
  id: number;

  @Column({
    type: 'int',
    name: 'flag_card_id',
  })
  flagCardId: number;

  @Column({
    type: 'int',
    name: 'payment_type',
  })
  paymentType: number;

  @Column({
    type: 'int',
    name: 'account_id',
  })
  accountId: number;

  @Column({
    type: 'int',
    name: 'installment_number',
  })
  installmentNumber: number;

  @Column({
    type: 'decimal',
    name: 'fee_value',
  })
  feeValue: string;

  @Column({
    type: 'timestamp',
    name: 'updated_at',
  })
  updatedAt: string;

  @Column({
    type: 'timestamp',
    name: 'created_at',
  })
  createdAt: string;

  @Column({
    type: 'int',
    name: 'active',
    default: 0,
  })
  active: number;
}
