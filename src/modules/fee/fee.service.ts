import { Injectable } from '@nestjs/common';
import { Fee } from '@fee/fee.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class FeeService {
  constructor(@InjectRepository(Fee) private feeRepository: Repository<Fee>) {}

  async getAFee(filters: any) {
    return await this.feeRepository.findOne(filters, {
      order: {
        updatedAt: 'DESC',
      },
    });
  }
}
