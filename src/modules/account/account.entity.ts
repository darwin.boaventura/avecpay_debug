import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Transaction } from '@transaction/transaction.entity';

@Entity()
export class Account {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
  })
  id: number;

  @Column('int', {
    nullable: false,
    name: 'account_type',
  })
  accountType: number;

  @Column('int', {
    nullable: false,
    default: () => '1',
    name: 'owner_legal_entity',
  })
  ownerLegalEntity: number;

  @Column('varchar', {
    nullable: false,
    length: 100,
    name: 'owner_name',
  })
  ownerName: string;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'owner_business_name',
  })
  ownerBusinessName: string | null;

  @Column('varchar', {
    nullable: false,
    length: 20,
    name: 'owner_document',
  })
  ownerDocument: string;

  @Column('varchar', {
    nullable: false,
    length: 100,
    name: 'owner_address',
  })
  ownerAddress: string;

  @Column('varchar', {
    nullable: false,
    length: 20,
    name: 'owner_address_number',
  })
  ownerAddressNumber: string;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'owner_address_complement',
  })
  ownerAddressComplement: string | null;

  @Column('varchar', {
    nullable: false,
    length: 50,
    name: 'owner_address_neighborhood',
  })
  ownerAddressNeighborhood: string;

  @Column('varchar', {
    nullable: false,
    length: 20,
    name: 'owner_address_postal_code',
  })
  ownerAddressPostalCode: string;

  @Column('varchar', {
    nullable: false,
    length: 50,
    name: 'owner_address_city',
  })
  ownerAddressCity: string;

  @Column('varchar', {
    nullable: false,
    length: 2,
    name: 'owner_address_state',
  })
  ownerAddressState: string;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'owner_home_phone',
  })
  ownerHomePhone: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'owner_business_phone',
  })
  ownerBusinessPhone: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'responsible_birth_date',
  })
  responsibleBirthDate: string | null;

  @Column('varchar', {
    nullable: true,
    name: 'responsible_gender',
  })
  responsibleGender: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'owner_mobile_phone',
  })
  ownerMobilePhone: string | null;

  @Column('varchar', {
    nullable: false,
    length: 100,
    name: 'owner_email',
  })
  ownerEmail: string;

  @Column('date', {
    nullable: true,
    name: 'owner_birth_date',
  })
  ownerBirthDate: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'owner_gender',
  })
  ownerGender: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'responsible_name',
  })
  responsibleName: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'responsible_document',
  })
  responsibleDocument: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'responsible_address',
  })
  responsibleAddress: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'responsible_address_number',
  })
  responsibleAddressNumber: string | null;

  @Column('varchar', {
    nullable: true,
    length: 100,
    name: 'responsible_address_complement',
  })
  responsibleAddressComplement: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'responsible_address_neighborhood',
  })
  responsibleAddressNeighborhood: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'responsible_address_postal_code',
  })
  responsibleAddressPostalCode: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'responsible_address_city',
  })
  responsibleAddressCity: string | null;

  @Column('varchar', {
    nullable: true,
    length: 2,
    name: 'responsible_address_state',
  })
  responsibleAddressState: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'responsible_home_phone',
  })
  responsibleHomePhone: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'responsible_business_phone',
  })
  responsibleBusinessPhone: string | null;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'responsible_mobile_phone',
  })
  responsibleMobilePhone: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'responsible_email',
  })
  responsibleEmail: string | null;

  @Column('varchar', {
    nullable: false,
    length: 50,
    name: 'financial_operation_key_1',
  })
  financialOperationKey1: string;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'financial_operation_key_2',
  })
  financialOperationKey2: string | null;

  @Column('varchar', {
    nullable: false,
    length: 50,
    name: 'parent_financial_operation_key',
  })
  parentFinancialOperationKey: string;

  @Column('varchar', {
    nullable: true,
    length: 20,
    name: 'mcc_code',
  })
  mccCode: string | null;

  @Column('tinyint', {
    nullable: false,
    default: 0,
    name: 'automatic_anticipation',
  })
  automaticAnticipation: number;

  @Column('tinyint', {
    nullable: false,
    default: 0,
    name: 'automatic_bank_tranference',
  })
  automaticBankTranference: number;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
  })
  updatedAt: Date;

  @Column('tinyint', {
    nullable: false,
    default: () => '1',
    name: 'active',
  })
  active: number;

  @Column('int', {
    name: 'allow_split',
  })
  allowSplit: number;

  // Relations
  @OneToMany(
    (type) => Transaction,
    (transaction) => transaction.account,
    {
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
    },
  )
  transactions: Transaction[];
}
