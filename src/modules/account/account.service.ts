import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from '@account/account.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AccountService {
  constructor(@InjectRepository(Account) private readonly accountRepository: Repository<Account>) {}

  async getAccountByZoopId(financialOperationKey2) {
    return await this.accountRepository.findOne({
      where: { financialOperationKey2 },
    });
  }
}
