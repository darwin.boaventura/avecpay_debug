import { Module } from '@nestjs/common';
import { AccountService } from '@account/account.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from '@account/account.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Account])],
  exports: [TypeOrmModule, AccountService],
  providers: [AccountService],
  controllers: [],
})
export class AccountModule {}
