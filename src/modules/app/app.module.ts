import { Module } from '@nestjs/common';
import { AccountModule } from '@account/account.module';
import { FeeModule } from '@fee/fee.module';
import { SplitModule } from '@split/split.module';
import { TransactionModule } from '@transaction/transaction.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ZoopModule } from '@zoop/zoop.module';
import { Account } from '@account/account.entity';
import { Fee } from '@fee/fee.entity';
import { Split } from '@split/split.entity';
import { Transaction } from '@transaction/transaction.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      entities: [Account, Fee, Split, Transaction],
      synchronize: false,
      url: process.env.DATABASE_URL,
      database: process.env.DATABASE_NAME,
    }),
    AccountModule,
    FeeModule,
    SplitModule,
    TransactionModule,
    ZoopModule,
  ],
  exports: [],
  controllers: [],
  providers: [],
})
export class AppModule {}
