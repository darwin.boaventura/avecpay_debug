export enum ZoopFlagCardENUM {
  'unknown' = 1,
  'MasterCard' = 2,
  'Visa' = 3,
  'Elo' = 4,
  'American Express' = 5,
  'Hipercard' = 6,
  'Visa Electron' = 7,
  'Hiper' = 8,
  'Maestro' = 9,
  'Banescard' = 10,
  'JCB' = 11,
  'Diners Club' = 12,
}
