import { Injectable, HttpService } from '@nestjs/common';

const { ZOOP_API_URL, ZOOP_MARKETPLACE } = process.env;

@Injectable()
export class ZoopService {
  protected readonly config: any;
  protected readonly baseURL: string;
  private readonly baseOptions: object;

  constructor(private readonly httpService: HttpService) {
    this.baseURL = `${ZOOP_API_URL}/v1/marketplaces/${ZOOP_MARKETPLACE}`;
    this.baseOptions = {
      headers: {
        'content-type': `application/json`,
      },
      auth: {
        username: process.env.ZOOP_AUTH_USERNAME,
        password: undefined,
      },
    };
  }

  async getReceivableByTransactionId(transactionId: string): Promise<any> {
    const url = `${this.baseURL}/transactions/${transactionId}/receivables`;

    const { data } = await this.httpService.get(url, this.baseOptions).toPromise();

    return data;
  }

  async getTransactionDetails(transactionId: string): Promise<any> {
    const url = `${this.baseURL}/transactions/${transactionId}`;

    const { data } = await this.httpService.get(url, this.baseOptions).toPromise();

    return data;
  }
}
