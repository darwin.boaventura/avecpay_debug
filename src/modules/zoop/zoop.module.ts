import { Module, HttpModule } from '@nestjs/common';
import { ZoopService } from '@zoop/zoop.service';

@Module({
  imports: [HttpModule],
  exports: [ZoopService],
  providers: [ZoopService],
  controllers: [],
})
export class ZoopModule {}
