import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Split } from '@split/split.entity';
import { Repository } from 'typeorm';

@Injectable()
export class SplitService {
  constructor(@InjectRepository(Split) private readonly splitRepository: Repository<Split>) {}

  async transactionHasSplit(id) {
    const split = await this.splitRepository.count({
      where: { transaction: { id } },
    });

    if (split) {
      return true;
    }

    return false;
  }
}
