import { Column, Entity, Index, JoinColumn, ManyToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { Transaction } from '@transaction/transaction.entity';

@Entity('transaction_split')
@Index('ix_transaction_split_transaction_id', ['transaction'])
export class Split {
  @PrimaryGeneratedColumn({
    type: 'int',
    name: 'id',
  })
  id: number;

  @ManyToOne(
    (type) => Transaction,
    (transaction) => transaction.transactionSplits,
    {
      nullable: false,
      onDelete: 'NO ACTION',
      onUpdate: 'NO ACTION',
    },
  )
  @JoinColumn({ name: 'transaction_id' })
  transaction: Transaction | null;

  @RelationId((transactionSplit: Split) => transactionSplit.transaction)
  transactionId: number[];

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'split_id',
  })
  splitId: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'resource',
  })
  resource: string | null;

  @Column('varchar', {
    nullable: true,
    length: 50,
    name: 'recipient',
  })
  recipient: string | null;

  @Column('tinyint', {
    nullable: false,
    name: 'deduct_processing_fee',
  })
  deductProcessingFee: number;

  @Column('tinyint', {
    nullable: false,
    name: 'deduct_recipient_processing_fee',
  })
  deductRecipientProcessingFee: number;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'transaction_split_percent',
  })
  transactionSplitPercent: string;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'transaction_split_value',
  })
  transactionSplitValue: string;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'receivable_value',
  })
  receivableValue: string;

  @Column('decimal', {
    nullable: false,
    default: () => '0.000000',
    precision: 18,
    scale: 6,
    name: 'receivable_gross_value',
  })
  receivableGrossValue: string;

  @Column('timestamp', {
    nullable: false,
    name: 'split_created_at',
  })
  splitCreatedAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'split_updated_at',
  })
  splitUpdatedAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'created_at',
  })
  createdAt: Date;

  @Column('timestamp', {
    nullable: false,
    name: 'updated_at',
  })
  updatedAt: Date;
}
