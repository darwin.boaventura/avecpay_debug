import { Module } from '@nestjs/common';
import { SplitService } from './split.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Split } from '@split/split.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Split])],
  exports: [SplitService, TypeOrmModule],
  providers: [SplitService],
  controllers: [],
})
export class SplitModule {}
