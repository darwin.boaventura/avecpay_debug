import { NestFactory } from '@nestjs/core';
import { AppModule } from '@app/app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  await app.listen(9020);

  console.log('App running on port 9020');
}
bootstrap();
